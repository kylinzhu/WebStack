# 感谢[viggo](http://viggoz.com)提供的源码,很不错的网址导航网站，也很实用，欢迎大家去给他star.[WebStackPage.github.io](https://github.com/WebStackPage/WebStackPage.github.io)
===

本项目这是一个**纯静态**的网址导航网站，内容均由[viggo](http://viggoz.com)收集并整理。项目基于bootstrap前端框架开发。

![](http://www.webstack.cc/assets/images/preview.gif)

这是一个开源的公益项目，你可以拿来制作自己的网址导航，也可以做与导航无关的网站。如果你有任何疑问，可以通过个人网站[viggoz.com](http://viggoz.com)中的联系方式找到我，欢迎与我交流分享。

怎么用?
---
你可以像我一样直接使用静态托管形式，如果你需要一个后台系统方便管理，可以参考下面的第二第三种解决方案：
#### 方法1. 使用静态托管
最简单快速上线自己的导航网站，你可以直接**下载**本项目修改内容既可部署上线。

#### 方法2. 使用基于 Laravel 搭建的后台系统🔥(感谢[@hui-ho](https://github.com/hui-ho)提供)
开源地址：https://github.com/hui-ho/WebStack-Laravel

Docker部署版本:https://hub.docker.com/r/arvon2014/webstack-laravel

#### 方法3. WordPress 主题🔥(感谢 [@一为忆](https://www.iowen.cn/)提供)
开源地址：https://github.com/owen0o0/WebStack

教程：[WordPress 版 WebStack 导航主题使用说明 \| 一为忆](https://www.iowen.cn/wordpress-version-webstack/)

#### 方法4. 基于Java开发的后台系统🔥(感谢[@jsnjfz](https://github.com/jsnjfz)提供)
开源地址：https://github.com/jsnjfz/WebStack-Guns

#### 方法5. 使用 Jekyll 版本的后台🔥(感谢[@0xl2oot](https://github.com/0xl2oot)提供)
开源地址：[https://github.com/0xl2oot/webstack-jekyll](https://github.com/0xl2oot/webstack-jekyll)

#### 方法6. 使用Typecho主题🔥(感谢[@SEOGO](https://www.seogo.me/)提供)
开源地址：[https://www.seogo.me/muban/webstack.html](https://www.seogo.me/muban/webstack.html)

#### 方法7. 自己写后台系统
可以按照自己的喜好和框架搭建后台系统，也可以参考我设计好的后台框架自行搭建。本站设计开发过程在我的博客文章有详细讲到[《webstack \| viggo》](http://blog.viggoz.com/2018/01/03/2018-01-03-webstack/)。静态源码（半成品）：[https://github.com/WebStackPage/webstack-Admin](https://github.com/WebStackPage/webstack-Admin)

如果你有更好的解决方案，并且能够开源供大家使用，可以在本项目提Issus，或者直接通过我个人网站中的联系方式联系我。

JUST DOWNLOAD AND DO WHAT THE FUCK YOU WANT TO.



关于图片资源
---
```/assets/images/logos/default.png``` 这是网站标签的默认图标

```/assets/images/logos``` 这里是所有网站内的图标切图，尺寸均为120px*120px

```/assets/webstack_logos.sketch``` 这是网站标签收录的所有图标设计源文件，你可以在这里[下载](https://WebStackPage.github.io/assets/webstack_logos.sketch) 。打开前请确认Sketch版本高于50.2(55047)

## License

Copyright © 2017-2018 **[webstack.cc](https://webstack.cc)** Released under the **MIT License**.

> 注：本站开源的目的是大家能够在本站的基础之上有所启发，做出更多新的东西。并不是让大家照搬所有代码。
> 如果你使用这个开源项目，请**注明**本项目开源地址。
